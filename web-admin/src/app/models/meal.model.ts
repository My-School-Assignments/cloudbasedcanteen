export interface MealModel {
  id?: number;
  canteen?: number,
  name?: string,
  englishName?: string,
  imageBase64?: string,
  energy?: number,
  protein?: number,
  sacharids?: number,
  fats?: number,
  fiber?: number,
  alergens?: string
}
