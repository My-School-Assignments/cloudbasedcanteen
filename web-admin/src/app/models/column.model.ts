export class ColumnModel {
  name: string;
  type: 'STRING' | 'INTEGER' | 'IMAGE';
  property: string;
}
