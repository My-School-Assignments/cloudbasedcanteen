export interface CanteenModel {
  id?: number;
  name?: string;
  address?: string;
  openingHours?: string;
}
