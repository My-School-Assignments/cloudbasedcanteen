export interface SortModel {
  direction?: 'ASC' | 'DESC';
  property?: string;
  ignoreCase?: boolean;
  nullHandling?: string;
  descending?: boolean;
  ascending?: boolean;
}
