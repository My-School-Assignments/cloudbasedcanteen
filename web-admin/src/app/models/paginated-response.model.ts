import { SortModel } from './sort.model';

export interface PaginatedResponseModel<T> {
  content?: Array<T>;
  first?: boolean;
  last?: boolean;
  number?: number;
  numberOfElements?: number;
  size?: number;
  sort?: Array<SortModel>;
  totalElements?: number;
  totalPages?: number;
}
