import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ColumnModel } from "../../models/column.model";
import { ActionModel } from "../../models/action.model";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {

  @Input() data: Array<any> = [];
  @Input() columns: Array<ColumnModel> = [];
  @Input() actions: Array<ActionModel> = [];
  @Output() buttonClick = new EventEmitter<{action: string, row: any}>();

  constructor(private sanitizer: DomSanitizer) { }

  /**
   * Emit en event for the parent component
   * @param name
   * @param row
   */
  public fireAction(name: string, row: any) {
    this.buttonClick.emit({action: name, row: row});
  }

  /**
   * Sanitize the url string
   * @param image
   */
  public imageHelper(image: string) {
    return this.sanitizer.bypassSecurityTrustUrl(image);
  }
}
