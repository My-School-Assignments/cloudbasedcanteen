import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { CanteenModel } from "../models/canteen.model";

@Injectable({
  providedIn: 'root'
})
export class CanteenService {

  private options = {
    headers: new HttpHeaders({
      'content-type': 'application/json'
    })
  };

  constructor(private http: HttpClient) {}

  public getCanteens() {
    return this.http.get<Array<CanteenModel>>(`${environment.apiBasePath}canteen`, this.options);
  }

  public createCanteen(canteen: CanteenModel) {
    return this.http.post<CanteenModel>(`${environment.apiBasePath}canteen`, canteen, this.options);
  }

  public editCanteen(id: number, canteen: CanteenModel) {
    return this.http.put<CanteenModel>(`${environment.apiBasePath}canteen/${id}`, canteen, this.options);
  }

  public deleteCanteen(id: number) {
    return this.http.delete<CanteenModel>(`${environment.apiBasePath}canteen/${id}`, this.options);
  }
}
