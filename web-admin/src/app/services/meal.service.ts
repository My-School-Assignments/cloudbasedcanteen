import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { MealModel } from "../models/meal.model";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MealService {

  private options = {
    headers: new HttpHeaders({
      'content-type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  public getMeals() {
    return this.http.get<Array<MealModel>>(`${environment.apiBasePath}meal`, this.options);
  }

  public createMeal(meal: MealModel): Observable<MealModel> {
    return this.http.post<MealModel>(`${environment.apiBasePath}meal`, {meal: meal}, this.options);
  }

  public editMeal(id: number, meal: MealModel) {
    return this.http.put<MealModel>(`${environment.apiBasePath}meal/${id}`, meal, this.options);
  }

  public deleteMeal(id: number) {
    return this.http.delete(`${environment.apiBasePath}meal/${id}`, this.options);
  }
}
