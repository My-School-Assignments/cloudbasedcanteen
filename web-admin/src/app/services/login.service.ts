import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public loggedIn: BehaviorSubject<boolean> = new BehaviorSubject(false);

  public login(username: string, password: string) {
    if (username === 'admin' && password === 'admin') {
      this.loggedIn.next(true);
    }
  }

  public logout() {
    this.loggedIn.next(false);
  }
}
