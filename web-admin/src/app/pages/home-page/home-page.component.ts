import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CanteenModel } from '../../models/canteen.model';
import { CanteenService } from '../../services/canteen.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MealModel } from "../../models/meal.model";
import { MealService } from "../../services/meal.service";
import { ActionModel } from "../../models/action.model";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  // reference to the form for automatic scrolling
  @ViewChild('mealEditRef') mealEditRef: ElementRef;
  // table configs
  public mealColumns = [
    {
      name: 'IMAGE',
      property: 'imageBase64',
      type: 'IMAGE'
    },
    {
      name: 'MEAL NAME',
      property: 'name',
      type: 'STRING'
    },
    {
      name: 'ENGLISH NAME',
      property: 'englishName',
      type: 'STRING'
    },
    {
      name: 'CALORIES',
      property: 'energy',
      type: 'INTEGER'
    },
    {
      name: 'PROTEIN',
      property: 'protein',
      type: 'INTEGER'
    },
    {
      name: 'CARBS',
      property: 'sacharids',
      type: 'INTEGER'
    },
    {
      name: 'FATS',
      property: 'fats',
      type: 'INTEGER'
    },
    {
      name: 'FIBER',
      property: 'fiber',
      type: 'INTEGER'
    },
    {
      name: 'ALERGENS',
      property: 'alergens',
      type: 'STRING'
    }
  ];
  public columns = [
    {
      name: 'CANTEEN NAME',
      property: 'name',
      type: 'STRING'
    },
    {
      name: 'ADDRESS',
      property: 'address',
      type: 'STRING'
    },
    {
      name: 'OPENING HOURS',
      property: 'openingHours',
      type: 'STRING'
    }
  ];
  public actions: Array<ActionModel> = [
    {
      name: 'edit',
      icon: 'edit'
    },
    {
      name: 'delete',
      icon: 'delete'
    },
    {
      name: 'detail',
      icon: 'search'
    }
  ];
  public mealActions: Array<ActionModel> = [
    {
      name: 'edit',
      icon: 'edit'
    },
    {
      name: 'delete',
      icon: 'delete'
    }
  ];
  public mealAllData: MealModel[] = []; // all meal data
  public mealData: MealModel[] = []; // meals for the currently selected canteen
  public data: CanteenModel[] = []; // canteens
  public editMealMode = false;
  public showMealEditCard = false;
  public editMode = false; // true if editing a canteen instead of adding a new one
  public showEditCard = false;
  public form: FormGroup;
  public mealForm: FormGroup;
  public selectedCanteen: CanteenModel = null;
  private editedCanteen: CanteenModel = null;
  private editedMeal: MealModel = null;
  private uploadedImage: string = null;

  constructor(private formBuilder: FormBuilder, private canteenService: CanteenService,
              private mealService: MealService) {}

  ngOnInit() {
    // init forms
    this.form = this.formBuilder.group({
      id: [null],
      name: [null, [Validators.required]],
      address: [null, [Validators.required]],
      openingHours: [null, [Validators.required]]
    });
    this.mealForm = this.formBuilder.group({
      id: [null],
      name: [null, [Validators.required]],
      englishName: [null, [Validators.required]],
      energy: [null, [Validators.required]],
      protein: [null, [Validators.required]],
      sacharids: [null, [Validators.required]],
      fats: [null, [Validators.required]],
      fiber: [null, [Validators.required]],
      alergens: [null]
    });
    // get canteens from API
    this.getCanteens();
    // get meals from API
    this.mealService.getMeals().subscribe(
      (response: Array<MealModel>) => {
        this.mealAllData = response;
      }
    );
  }

  /**
   * Fetches all canteens from backend
   */
  public getCanteens() {
    this.canteenService.getCanteens().subscribe(
      (response: Array<CanteenModel>) => {
        this.data = response;
      }
    )
  }

  /**
   * Handle actions from the canteen table rows
   * @param event
   */
  public handleAction(event: {action: string, row: CanteenModel}) {
    switch (event.action) {
      case 'edit': this.editCanteen(event.row); break;
      case 'delete': this.deleteCanteen(event.row); break;
      case 'detail': this.showDetail(event.row);
    }
  }

  /**
   * Handle actions from the meal table rows
   * @param event
   */
  public handleMealAction(event: {action: string, row: MealModel}) {
    switch (event.action) {
      case 'edit': this.editMeal(event.row); break;
      case 'delete': this.deleteMeal(event.row); break;
    }
  }

  /**
   * Open a card with a form for adding a new canteen
   */
  public addCanteen() {
    this.showEditCard = true;
    this.editMode = false;
  }

  /**
   * Open a card with a form for adding a new meal
   */
  public addMeal() {
    this.showMealEditCard = true;
    this.editMealMode = false;
    setTimeout(() => {
      this.mealEditRef.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
    }, 5);
  }

  /**
   * Opens a card and fills out the edit form for editing of a canteen
   * @param canteen
   */
  public editCanteen(canteen: CanteenModel) {
    this.showEditCard = true;
    this.editMode = true;
    this.editedCanteen = canteen;
    this.form.patchValue({
      ...canteen
    });
  }

  /**
   * Deletes a canteen
   * @param canteen
   */
  public deleteCanteen(canteen: CanteenModel) {
    this.canteenService.deleteCanteen(canteen.id).subscribe(
      () => {
        this.data.splice(this.data.indexOf(canteen), 1);
      }
    );
  }

  /**
   * Opens a card with a table showing meals for the canteen
   * @param canteen
   */
  public showDetail(canteen: CanteenModel) {
    this.selectedCanteen = canteen;
    this.mealData = this.mealAllData.filter(
      (meal: MealModel) => meal.canteen === canteen.id);
  }

  /**
   * Confirm the addition or editing of a canteen
   */
  public confirm() {
    if (!this.form.valid)
      return;
    if (this.editMode) {
      const newCanteen: CanteenModel = {
        name: this.form.value.name,
        address: this.form.value.address,
        openingHours: this.form.value.openingHours
      };
      this.canteenService.editCanteen(this.editedCanteen.id, newCanteen).subscribe(
        () => {
          this.editedCanteen.name = newCanteen.name;
          this.editedCanteen.address = newCanteen.address;
          this.editedCanteen.openingHours = newCanteen.openingHours;
          this.showEditCard = false;
        }
      );
    } else {
      const newCanteen: CanteenModel = {
        name: this.form.value.name,
        address: this.form.value.address,
        openingHours: this.form.value.openingHours
      };
      this.canteenService.createCanteen(newCanteen).subscribe(
        () => {
          this.getCanteens();
          this.showEditCard = false;
        }
      );
    }
  }

  /**
   * Close the canteen editing card
   */
  public cancel() {
    this.showEditCard = false;
  }

  /**
   * Open a card, with a form for editing of a meal
   * @param meal
   */
  public editMeal(meal: MealModel) {
    this.showMealEditCard = true;
    this.editMealMode = true;
    this.editedMeal = meal;
    this.uploadedImage = meal.imageBase64;
    this.mealForm.patchValue({
      ...meal
    });
    setTimeout(() => {
      this.mealEditRef.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
    }, 5);
  }

  /**
   * Delete the specified meal
   * @param meal
   */
  public deleteMeal(meal: MealModel) {
    this.mealService.deleteMeal(meal.id).subscribe(
      () => {
        this.mealData.splice(this.mealData.indexOf(meal), 1);
      }
    );
  }

  /**
   * Confirm editing or adding of a new meal
   */
  public confirmMeal() {
    if (!this.mealForm.valid)
      return;
    if (this.editMealMode) {
      const newMeal: MealModel = {
        id: 0,
        name: this.mealForm.value.name,
        englishName: this.mealForm.value.englishName,
        energy: this.mealForm.value.energy,
        protein: this.mealForm.value.protein,
        sacharids: this.mealForm.value.sacharids,
        fats: this.mealForm.value.fats,
        fiber: this.mealForm.value.fiber,
        alergens: this.mealForm.value.alergens,
        imageBase64: this.uploadedImage
      };
      this.mealService.editMeal(this.editedMeal.id, newMeal).subscribe(
        () => {
          this.editedMeal.name = newMeal.name;
          this.editedMeal.englishName = newMeal.englishName;
          this.editedMeal.energy = newMeal.energy;
          this.editedMeal.protein = newMeal.protein;
          this.editedMeal.sacharids = newMeal.sacharids;
          this.editedMeal.fats = newMeal.fats;
          this.editedMeal.fiber = newMeal.fiber;
          this.editedMeal.alergens = newMeal.alergens;
          this.editedMeal.imageBase64 = this.uploadedImage;
          this.showMealEditCard = false;
        }
      );
    } else {
      const newMeal: MealModel = {
        id: 0,
        canteen: this.selectedCanteen.id,
        name: this.mealForm.value.name,
        englishName: this.mealForm.value.englishName,
        energy: this.mealForm.value.energy,
        protein: this.mealForm.value.protein,
        sacharids: this.mealForm.value.sacharids,
        fats: this.mealForm.value.fats,
        fiber: this.mealForm.value.fiber,
        alergens: this.mealForm.value.alergens,
        imageBase64: this.uploadedImage
      };
      this.mealService.createMeal(newMeal).subscribe(
        (response) => {
          this.showMealEditCard = false;
          this.mealAllData.push(response);
          this.mealData = this.mealAllData.filter(
                (meal: MealModel) => meal.canteen === this.selectedCanteen.id);
        }
      );
    }
  }

  /**
   * Close the editing card
   */
  public cancelMeal() {
    this.showMealEditCard = false;
  }

  /**
   * Convert the file from the file input element to a string and save it to local variable
   * @param event
   */
  public handleFileInput(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
          this.uploadedImage = reader.result;
      };
    }
  }
}
