import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  public form: FormGroup;
  private loginSubscription = Subscription.EMPTY;

  constructor(private loginService: LoginService,
              private router: Router,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: [null],
      password: [null]
    });
    this.loginSubscription = this.loginService.loggedIn.subscribe(
      (value) => {
        if (value) {
          this.router.navigate(['home']);
        }
      }
    );
  }

  /**
   * Log in with given credentials
   */
  public login() {
    this.loginService.login(this.form.value.username, this.form.value.password);
  }

}
