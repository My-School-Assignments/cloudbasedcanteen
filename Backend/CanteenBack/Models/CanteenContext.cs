﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CanteenBack.Models
{
    public class CanteenContext : DbContext
    {
        public CanteenContext(DbContextOptions<CanteenContext> options)
            : base(options)
        { }

        public DbSet<Canteen> Canteens { get; set; }
        public DbSet<Meal> Meals { get; set; }
    }
}
