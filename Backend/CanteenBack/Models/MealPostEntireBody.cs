﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenBack.Models
{
    // Model used to distinguish whether user wants to add meal to DB or get meals from picture
    public class MealPostEntireBody
    {
        public Meal meal { get; set; }
        public PictureRequestBody pictureBody { get; set; }
    }
}
