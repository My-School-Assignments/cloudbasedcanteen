﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenBack.Models
{
    // Model for PictureRequest body
    public class PictureRequestBody
    {
        public string ImageInBase64 { get; set; }
        public DateTime dateTime { get; set; }
    }
}
