﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenBack.Models
{
    // Model representing meal
    public class Meal
    {
        public int Id { get; set; }
        public int Canteen { get; set; }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string ImageBase64 { get; set; }
        public float Energy { get; set; }
        public float Protein { get; set; }
        public float Sacharids { get; set; }
        public float Fats { get; set; }
        public float Fiber { get; set; }
        public string Alergens { get; set; }
    }
}
