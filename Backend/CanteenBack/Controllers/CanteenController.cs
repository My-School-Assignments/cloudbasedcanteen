﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CanteenBack.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CanteenBack.Controllers
{
    [Route("api/[controller]")]
    public class CanteenController : ControllerBase
    {
        private readonly CanteenContext _context;

        public CanteenController(CanteenContext context)
        {
            _context = context;
        }

        // GET: api/Canteen
        // returns all canteens
        [HttpGet]
        public List<Canteen> GetAll()
        {
            return _context.Canteens.ToList();
        }

        // GET: api/Canteen/5
        // returns canteen by ID
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var item = _context.Canteens.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        // POST: api/Canteen
        // Adds canteen to DB
        [HttpPost]
        public IActionResult Create([FromBody] Canteen item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _context.Canteens.Add(item);
            _context.SaveChanges();

            return Created("api/canteen/" + item.Id, item);
        }

        // PUT: api/Canteen/5
        // Updates canteen
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Canteen value)
        {
            if (value == null)
            {
                return BadRequest();
            }

            var canteen = _context.Canteens.Find(id);
            canteen.Address = value.Address;
            canteen.Name = value.Name;
            canteen.OpeningHours = value.OpeningHours;

            _context.SaveChanges();

            return Created("api/canteen/" + value.Id, value);
        }

        // DELETE: api/Canteen/5
        // Removes canteen
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var canteen = _context.Canteens.Find(id);

            if(canteen == null)
            {
                return BadRequest();
            }

            _context.Canteens.Remove(canteen);
            _context.SaveChanges();

            return Created("DELETED: api/canteen/" + id, canteen);
        }
    }
}
