﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CanteenBack.Models;
using Microsoft.AspNetCore.Cors;
using System.Net.Http;
using System.Web;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Globalization;

namespace CanteenBack.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MealController : ControllerBase
    {
        private readonly CanteenContext _context;

        public MealController(CanteenContext context)
        {
            _context = context;
        }

        // GET: api/Meal
        // returns all the meals
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_context.Meals.ToList());
        }

        // gets list of meal names from picture
        private List<string> mealsFromPicture(string pictureBase64)
        {
            byte[] byteData = Convert.FromBase64String(pictureBase64);
            List<string> finalMeals = new List<string>();
            List<string> meals = new List<string>();
            const string subscriptionKey = "90723c117b2b4bc18bafbbc8ad9a5067";
            const string uriBase = "https://westeurope.api.cognitive.microsoft.com/vision/v2.0/ocr";

            try
            {
                HttpClient client = new HttpClient();
                var queryString = HttpUtility.ParseQueryString(string.Empty);

                // Request headers.
                client.DefaultRequestHeaders.Add(
                    "Ocp-Apim-Subscription-Key", subscriptionKey);

                // Request parameters. A third optional parameter is "details".
                // The Analyze Image method returns information about the following
                // visual features:
                // Categories:  categorizes image content according to a
                //              taxonomy defined in documentation.
                // Description: describes the image content with a complete
                //              sentence in supported languages.
                // Color:       determines the accent color, dominant color, 
                //              and whether an image is black & white.
                queryString["language"] = "unk";
                queryString["detectOrientation "] = "true";

                // Assemble the URI for the REST API method.
                string uri = uriBase + "?" + queryString;

                HttpResponseMessage response;

                // Add the byte array as an octet stream to the request body.
                using (ByteArrayContent content = new ByteArrayContent(byteData))
                {
                    // This example uses the "application/octet-stream" content type.
                    // The other content types you can use are "application/json"
                    // and "multipart/form-data".
                    content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/octet-stream");

                    // Asynchronously call the REST API method.
                    response = client.PostAsync(uri, content).Result;
                }

                // Asynchronously get the JSON response.
                string contentString = response.Content.ReadAsStringAsync().Result;

                // Display the JSON response.
                Console.WriteLine("\nResponse:\n\n{0}\n",
                    JToken.Parse(contentString).ToString());

                JObject jsonik = JObject.Parse(contentString);
                foreach (var region in jsonik["regions"])
                {
                    foreach (var line in region["lines"])
                    {
                        string lineText = "";
                        foreach (var word in line["words"])
                        {
                            //meals.Add((string)word["text"]);
                            if (!(((string)word["text"]).Any(c => char.IsDigit(c))))
                            {
                                lineText = lineText + (string)word["text"] + " ";
                            }
                        }                       
                        if (lineText.Length > 0)
                        {
                            lineText = lineText.Substring(0, lineText.Length - 1);
                            meals.Add(lineText);
                        }
                        
                    }
                }
                var mealsFromDB = _context.Meals.ToList();
                foreach (var mealFromDB in mealsFromDB)
                {
                    foreach(var meal in meals)
                    {
                        if (RemoveDiacritics(mealFromDB.Name).Contains(RemoveDiacritics(meal)))
                        {
                            if (finalMeals.Contains(mealFromDB.Name) == false)
                            {
                                finalMeals.Add(mealFromDB.Name);
                            }
                        }
                    }
                }
                return finalMeals;
            }
            catch (Exception e)
            {
                Console.WriteLine("\n" + e.Message);
                return null;
            }
        }

        // removes non ascii characters from string
        static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        // GET: api/Meal/5
        // Returns meal based on ID
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var item = _context.Meals.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        // POST: api/Meal
        // Either adds meal to DB or returns all meals on the picture posted
        [HttpPost]
        public IActionResult Post([FromBody] MealPostEntireBody value)
        {
            if (value == null)
            {
                return BadRequest();
            }

            if (value.meal != null)
            {
                _context.Meals.Add(value.meal);
                _context.SaveChanges();

                return Created("api/Meal/" + value.meal.Id, value.meal);
            }

            if(value.pictureBody != null)
            {
                var mealStrings = mealsFromPicture(value.pictureBody.ImageInBase64);
                if (mealStrings == null) return NotFound();
                var outputMeals = new List<Meal>();

                foreach (var mealString in mealStrings)
                {
                    var meal = _context.Meals.First<Meal>(m => m.Name == mealString);
                    outputMeals.Add(meal);
                }

                return Ok(outputMeals);
            }

            return BadRequest();
        }

        // PUT: api/Meal/5
        // Updates meal
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Meal value)
        {
            if (value == null)
            {
                return BadRequest();
            }

            var meal = _context.Meals.Find(id);
            meal.Alergens = value.Alergens;
            meal.Energy = value.Energy;
            meal.Fats = value.Fats;
            meal.Fiber = value.Fiber;
            meal.ImageBase64 = value.ImageBase64;
            meal.Name = value.Name;
            meal.EnglishName = value.EnglishName;
            meal.Protein = value.Protein;
            meal.Sacharids = value.Sacharids;

            _context.SaveChanges();

            return Created("api/canteen/" + value.Id, value);
        }

        // DELETE: api/ApiWithActions/5
        // Deletes meal
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var meal = _context.Meals.Find(id);

            if (meal == null)
            {
                return BadRequest();
            }

            _context.Meals.Remove(meal);
            _context.SaveChanges();

            return Created("DELETED: api/canteen/" + id, meal);
        }
    }
}
