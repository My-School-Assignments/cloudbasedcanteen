﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CanteenBack.Migrations
{
    public partial class Models2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Canteen",
                table: "Meals",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Canteen",
                table: "Meals");
        }
    }
}
