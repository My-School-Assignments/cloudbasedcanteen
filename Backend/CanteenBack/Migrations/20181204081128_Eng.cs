﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CanteenBack.Migrations
{
    public partial class Eng : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EnglishName",
                table: "Meals",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EnglishName",
                table: "Meals");
        }
    }
}
