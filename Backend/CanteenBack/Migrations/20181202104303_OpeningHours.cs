﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CanteenBack.Migrations
{
    public partial class OpeningHours : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Canteens",
                table: "Canteens");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "Canteens",
                nullable: false,
                oldClrType: typeof(long))
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                .OldAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<string>(
                name: "OpeningHours",
                table: "Canteens",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Canteens",
                table: "Canteens",
                columns: new[] { "Id" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Canteens",
                table: "Canteens");

            migrationBuilder.DropColumn(
                name: "OpeningHours",
                table: "Canteens");

            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "Canteens",
                nullable: false,
                oldClrType: typeof(int))
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                .OldAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Canteens",
                table: "Canteens",
                columns: new[] { "Id" });
        }
    }
}
