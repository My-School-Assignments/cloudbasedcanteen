package com.kkuirulz.cloudcanteen

import com.kkuirulz.cloudcanteen.networking.MealResponseModel

object FoodDataSingleton {

    var data: Array<MealResponseModel>? = null
    var canteenIndex: Int? = null

    fun updateCanteenName(){
        if (data == null) return
        val indexes = arrayListOf<Int>()
        for (meal in data!!) {
            if (meal.canteen != null) {
                indexes.add(meal.canteen)
            }
        }
        canteenIndex = modeOf(indexes)
    }

    private fun <T> modeOf(a: ArrayList<T>): T {
        val sortedByFreq = a.groupBy { it }.entries.sortedByDescending { it.value.size }
        val maxFreq = sortedByFreq.first().value.size
        val modes = sortedByFreq.takeWhile { it.value.size == maxFreq }
        return modes.first().key
    }
}
