package com.kkuirulz.cloudcanteen.networking

data class CanteenModel (
    val id: Int,
    val name: String,
    val address: String,
    val openingHours: String
)