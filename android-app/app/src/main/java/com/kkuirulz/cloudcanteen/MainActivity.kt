package com.kkuirulz.cloudcanteen

import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.view.View
import android.view.animation.AnimationUtils
import com.kkuirulz.cloudcanteen.networking.CanteenApi
import com.kkuirulz.cloudcanteen.networking.MealRequestModel
import com.kkuirulz.cloudcanteen.networking.PictureBody
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val REQUEST_IMAGE_CAPTURE = 1
    private var disposable: Disposable? = null

    private var counter = 0

    private val canteenApi by lazy {
        CanteenApi.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainWaitOverlay.visibility = View.GONE
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mainWaitOverlay.elevation = 1000F
        }
    }


    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            photoPreview.setImageBitmap(imageBitmap)

            // Animated show wait indicator
            showLoader()

            // Prepare data for request
            val imageString = bitmapToBase64String(imageBitmap, Bitmap.CompressFormat.JPEG, 100)
            val timeStrig = getTimeString()
            val pictureBody = PictureBody(imageString, timeStrig)
            val mealRequest = MealRequestModel(pictureBody)

            // Request
            disposable = canteenApi.postMealWithPicture(mealRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { result ->
                                FoodDataSingleton.data = result
                                if (result.isNotEmpty()) {
                                    val intent = Intent(this, FoodListActivity::class.java)
                                    startActivity(intent)
                                } else {
                                    hideLoader()
                                    showAlert(resources.getString(R.string.sorry), resources.getString(R.string.noData))
                                }
                            },
                            { error ->
                                hideLoader()
                                showErrorAlert(error)
                            }
                    )
        } else {
            mainWaitOverlay.visibility = View.GONE
            showAllButton.visibility = View.GONE
        }
    }


    fun takePicture(v: View) {
        dispatchTakePictureIntent()
    }


    fun testButtonPressed(v: View) {
        // Vsetky jedla
        showLoader()
        disposable = canteenApi.getAllMeals()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                    { result ->
                        mainWaitOverlay.clearAnimation()
                        FoodDataSingleton.data = result
                        FoodDataSingleton.updateCanteenName()
                        val intent = Intent(this, FoodListActivity::class.java)
                        startActivity(intent)
                    },
                    { error ->
                        hideLoader()
                        showErrorAlert(error)
                    }
            )
    }


    private fun showAlert(title: String, message: String) {
        AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK",null)
                .create()
                .show()
    }


    private fun showErrorAlert(error: Throwable) {
        showAlert(resources.getString(R.string.error), error.localizedMessage + "\n\n"
                + resources.getString(R.string.tryAgain))
    }


    private fun showLoader() {
        val fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.loader_fade_id)
        mainWaitOverlay.visibility = View.VISIBLE
        mainWaitOverlay.startAnimation(fadeInAnimation)
    }


    private fun hideLoader() {
        mainWaitOverlay.clearAnimation()
        mainWaitOverlay.visibility = View.GONE
    }


    override fun onPause() {
        super.onPause()
        disposable?.dispose()
        mainWaitOverlay.visibility = View.GONE
    }

    fun michal(v: View){
        showAlert("","Android developer:\n\nMichal Géci")
    }

    fun martin(v: View) {
        showAlert("","Web admin developer:\n\nMartin Liščinský")
    }

    fun richard(v: View) {
        showAlert("","AI developer:\n\nRichard Halčin")
    }

    fun dominik(v: View) {
        showAlert("", "Back-End develper:\n\nDominik Horňák")
    }

    fun schoolButtonPressed(v: View) {
        if (counter > 6) {
            showAllButton.visibility = View.VISIBLE
        } else {
            counter += 1
        }
    }

    fun restaurantButtonPressed(v: View) {
        counter = 0
        showAllButton.visibility = View.GONE
    }
}
