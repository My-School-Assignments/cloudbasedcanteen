package com.kkuirulz.cloudcanteen.networking

data class MealResponseModel(
    val id: Int,
    val canteen: Int?,
    val name: String?,
    val englishName: String?,
    val imageBase64: String?,
    val energy: Float?,
    val protein: Float?,
    val sacharids: Float?,
    val fats: Float?,
    val fiber: Float?,
    val alergens: String?
)