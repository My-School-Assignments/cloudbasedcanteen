package com.kkuirulz.cloudcanteen.networking

import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface CanteenApi {

    @GET("meal")
    fun getAllMeals(): Observable<Array<MealResponseModel>>

    @Headers("Content-Type: application/json")
    @POST("meal")
    fun postMealWithPicture(@Body mealRequestModel: MealRequestModel): Observable<Array<MealResponseModel>>

    @GET("canteen/{id}")
    fun getCanteenById(@Path("id") id: Int): Observable<CanteenModel>

    companion object Factory {
        fun create(): CanteenApi {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY;
            val client = OkHttpClient.Builder()
            client.addInterceptor(logging)
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://canteenbackend.azurewebsites.net/api/")
                    .client(client.build())
                    .build()

            return retrofit.create(CanteenApi::class.java);
        }
    }

}