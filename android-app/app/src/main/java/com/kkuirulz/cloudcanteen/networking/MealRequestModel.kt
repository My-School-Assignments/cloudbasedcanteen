package com.kkuirulz.cloudcanteen.networking

data class PictureBody(
        val imageInBase64: String,
        val dateTime: String
)

data class MealRequestModel(
    val pictureBody: PictureBody
)