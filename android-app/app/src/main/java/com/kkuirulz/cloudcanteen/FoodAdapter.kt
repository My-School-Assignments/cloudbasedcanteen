package com.kkuirulz.cloudcanteen

import android.app.AlertDialog
import android.content.Context
import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.kkuirulz.cloudcanteen.networking.MealResponseModel
import jp.wasabeef.blurry.Blurry
import org.jetbrains.anko.doAsync

class FoodAdapter(val data: Array<MealResponseModel>) : RecyclerView.Adapter<FoodAdapter.ViewHolder>() {

    var resources: Resources? = null
    private var myContext: Context? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.food_row, parent, false)
        myContext = parent.context
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        return data.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val meal = data[position]

        // Set name of meal
        holder.foodName.text = meal.name
        holder.englishName.text = meal.englishName

        // Set placeholder image
        holder.imageView.setImageResource(R.drawable.ic_local_dining_black_24dp)
        holder.backgroundImage.setImageResource(R.drawable.gray_solid)

        // Async replace of placeholder image
        doAsync {
            val imageString = meal.imageBase64 ?: return@doAsync
            val image = base64StringToBitmap(imageString)
            holder.imageView.setImageBitmap(image)
            Blurry.with(myContext).from(image).into(holder.backgroundImage)
        }

        // Get strings from resources
        val englishName = resources?.getString(R.string.englishName)
        val energy = resources?.getString(R.string.energy)
        val proteins = resources?.getString(R.string.proteins)
        val sacharids = resources?.getString(R.string.sacharids)
        val fats = resources?.getString(R.string.fats)
        val fiber = resources?.getString(R.string.fiber)
        val alergens = resources?.getString(R.string.alergens)

        var formattedAlergens = meal.alergens
        formattedAlergens = formattedAlergens?.replace(";", "\n")

        // Set on click listener to show alert
        holder.itemView.setOnClickListener {
            val alertBuilder = AlertDialog.Builder(myContext)
            alertBuilder.setTitle(meal.name)
            alertBuilder.setCancelable(false)
            alertBuilder.setPositiveButton("OK",null)
            alertBuilder.setMessage(
                englishName + ":\n"
                + meal.englishName + "\n"
                + "\n"
                + energy + ": " + meal.energy.toString() + " kcal\n"
                + proteins +": " + meal.protein.toString() + " g\n"
                + sacharids + ": " + meal.sacharids.toString() + " g\n"
                + fats + ": " + meal.fats.toString() + " g\n"
                + fiber + ": " + meal.fiber.toString() + " g\n"
                + "\n"
                + alergens + ":\n"
                + formattedAlergens
            )
            alertBuilder.create().show()
        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.findViewById(R.id.foodPrewiew)
        val foodName: TextView = itemView.findViewById(R.id.mealName)
        val englishName: TextView = itemView.findViewById(R.id.mealEnglishName)
        val backgroundImage: ImageView = itemView.findViewById(R.id.backgroundImage)
    }

}