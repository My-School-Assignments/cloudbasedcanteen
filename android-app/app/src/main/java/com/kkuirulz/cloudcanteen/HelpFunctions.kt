package com.kkuirulz.cloudcanteen

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

fun getTimeString(): String {
    val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS", Locale.getDefault())
    dateFormatter.timeZone = TimeZone.getTimeZone("GMT")
    val offsetFormatter = SimpleDateFormat("Z", Locale.getDefault())

    val now = Calendar.getInstance(TimeZone.getTimeZone("GMT"))
    val nowDate = now.time

    val offset = offsetFormatter.format(nowDate)
    val nowString = dateFormatter.format(nowDate)

    return nowString + offset.substring(0, 3) + ":" + offset.substring(3, 5)
}

fun bitmapToBase64String(image: Bitmap, compressFormat: Bitmap.CompressFormat, quality: Int): String {
    val imageByteArray = ByteArrayOutputStream()
    image.compress(compressFormat, quality, imageByteArray)
    return android.util.Base64.encodeToString(imageByteArray.toByteArray(), android.util.Base64.DEFAULT)
}

fun base64StringToBitmap(base64: String): Bitmap {
    val pureBase64 = base64.substring(base64.indexOf(","))
    val decoded = android.util.Base64.decode(pureBase64, android.util.Base64.DEFAULT)
    return BitmapFactory.decodeByteArray(decoded, 0, decoded.size)
}

