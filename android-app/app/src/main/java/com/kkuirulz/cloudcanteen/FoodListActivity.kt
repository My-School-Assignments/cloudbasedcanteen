package com.kkuirulz.cloudcanteen

import android.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.kkuirulz.cloudcanteen.networking.CanteenApi
import com.kkuirulz.cloudcanteen.networking.CanteenModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_food_list.*

class FoodListActivity : AppCompatActivity() {

    private var disposable: Disposable? = null
    private val canteenApi by lazy {
        CanteenApi.create()
    }
    var canteen:CanteenModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_food_list)

        this.title = resources.getString(R.string.mealList)

        if (FoodDataSingleton.canteenIndex != null) {
            disposable = canteenApi.getCanteenById(FoodDataSingleton.canteenIndex!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { result ->
                                canteen = result
                                this.title = result.name + " " + result.address
                            },
                            { error ->
                                this.title = resources.getString(R.string.mealList)
                                Log.e("NetworkError", error.message)
                            }
                    )
        }

        foodRecyclerView.layoutManager = LinearLayoutManager(this)
        val foodAdapter = FoodAdapter(FoodDataSingleton.data!!)
        foodAdapter.resources = resources
        foodRecyclerView.adapter = foodAdapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.info_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        if (id == R.id.canteenInfoButton) {
            val alertBuilder = AlertDialog.Builder(this)
            alertBuilder.setTitle(canteen?.name)
            val address = resources.getString(R.string.address)
            val openingHours = resources.getString(R.string.openingHours)
            alertBuilder.setMessage(address + ":\n" + canteen?.address + "\n\n" +
                    openingHours + ":\n" + canteen?.openingHours)
            alertBuilder.setCancelable(false)
            alertBuilder.setPositiveButton("OK",null)
            alertBuilder.create().show()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }
}
